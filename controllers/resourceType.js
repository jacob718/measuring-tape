const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const ResourceType = require('../models/ResourceType');
const Measurement = require('../models/Measurement');

// @desc    Calculate resource
// @route   GET /resourcetype/:type/calculate?{params}
// @access  Public
exports.calculateResourceType = asyncHandler(async (req, res, next) => {
    // const vars = req.query;
    // const v1 = parseInt(vars.v1);
    // const v2 = parseInt(vars.v2);

    const resourceType = await ResourceType.findOne({"slug": req.params.type});

    const vars = [];

    resourceType.varNames.forEach(n => vars.push(req.query[n]));

    const measurement = await Measurement.findOne({"name": resourceType.measurement});

    const equation = resourceType.calculator
    const calculation = eval(equation);
    var result = eval(calculation);
    
    result = Math.round(result * 10) / 10;

    var resultMeas = measurement.singular;

    if(result != 1) {
        resultMeas = measurement.multiple
    }

    res.status(200).json({ test: vars, calculation: calculation, result: eval(result), 
        resultMeas: resultMeas, varNames: resourceType.varNames, varMeas: resourceType.varMeas });
});

// @desc    Get resource type
// @route   GET /resourcetype
// @access  Public
exports.getResourceTypes = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults);
});

// @desc    Get resource type names
// @route   GET /resourcetype/names
// @access  Public
exports.getResourceTypeNames = asyncHandler(async (req, res, next) => {
    const resources = await ResourceType.find();

    var names = [];

    for (var i = 0; i < resources.length; i ++) {
        names.push(resources[i].name);
    }

    res.status(200).json({names: names});
});

// @desc    Get single resource type
// @route   GET /resourcetype/:name
// @access  Public
exports.getResourceType = asyncHandler(async (req, res, next) => {
    // Find a resource type
    const resourceType = await ResourceType.findOne({"slug": req.params.name});

    // Check if name is in database
    if(!resourceType) {
        return next(new ErrorResponse(`Resource type not found with name of ${req.params.name}`, 404));
    }
        
    res.status(200).json( {success: true, data: resourceType}); 
});

// @desc    Create resource type
// @route   POST /resourcetype
// @access  Private
exports.createResourceType = asyncHandler(async (req, res, next) => {
    const resourceType = await ResourceType.create(req.body);  

    res.status(201).json({
        success: true,
        data: resourceType
    });
});

// @desc    Update resource type 
// @route   PUT /resourcetype/:name
// @access  Private
exports.updateResourceType = asyncHandler(async (req, res, next) => {

    const resourceType = await ResourceType.findOneAndUpdate({"slug": req.params.name}, req.body, {
        new: true,
        runValidators: true
    });

    if(!resourceType) {
        return next(new ErrorResponse(`Resource type not found with name of ${req.params.name}`, 404));
    }

    res.status(200).json({ success: true, data: resourceType });
});

// @desc    Delete resource type
// @route   DELETE /resourcetype/:name
// @access  Private
exports.deleteResourceType = asyncHandler(async (req, res, next) => {
    
    const resourceType = await ResourceType.findOne({"slug": req.params.name});

    if(!resourceType) {
        return next(new ErrorResponse(`Resource type not found with name of ${req.params.name}`, 404));
    }

    resourceType.remove();

    res.status(200).json({ success: true, data: {} });
});