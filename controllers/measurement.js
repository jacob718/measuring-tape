const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Measurement = require('../models/Measurement');

// @desc    Get measurement
// @route   GET /measurement
// @access  Public
exports.getMeasurements = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults);
});

// @desc    Get single measurement
// @route   GET /measurement/:name
// @access  Public
exports.getMeasurement = asyncHandler(async (req, res, next) => {
    // Find a measurement
    const measurement = await Measurement.findOne({"slug": req.params.name});

    // Check if name is in database
    if(!measurement) {
        return next(new ErrorResponse(`Measurement not found with name of ${req.params.name}`, 404));
    }
        
    res.status(200).json( {success: true, data: measurement}); 
});

// @desc    Create measurement
// @route   POST /measurement
// @access  Private
exports.createMeasurement = asyncHandler(async (req, res, next) => {
    const measurement = await Measurement.create(req.body);  

    res.status(201).json({
        success: true,
        data: measurement
    });
});

// @desc    Update measurement
// @route   PUT /measurement/:name
// @access  Private
exports.updateMeasurement = asyncHandler(async (req, res, next) => {

    const measurement = await Measurement.findOneAndUpdate({"slug": req.params.name}, req.body, {
        new: true,
        runValidators: true
    });

    if(!measurement) {
        return next(new ErrorResponse(`Measurement not found with name of ${req.params.name}`, 404));
    }

    res.status(200).json({ success: true, data: measurement });
});

// @desc    Delete measurement
// @route   DELETE /measurement/:name
// @access  Private
exports.deleteMeasurement = asyncHandler(async (req, res, next) => {
    const measurement = await Measurement.findOne({"slug": req.params.name});

    if(!measurement) {
        return next(new ErrorResponse(`Measurement not found with name of ${req.params.name}`, 404));
    }

    measurement.remove();

    res.status(200).json({ success: true, data: {} });
});