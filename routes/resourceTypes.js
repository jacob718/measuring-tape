const express = require('express');
const advancedResults = require('../middleware/advancedResults');

const ResourceType = require('../models/ResourceType');

const measurement = {
    path: 'measurement'
};
const opts = [ measurement ]

// Bring each method from resource type
const { 
    calculateResourceType,
    getResourceTypes,
    getResourceTypeNames,
    getResourceType,
    createResourceType,
    updateResourceType,
    deleteResourceType
 } 
    = require('../controllers/resourceType');

// mergeParams merges the rerouted params
const router = express.Router({ mergeParams: true });

router.route('/')
.get(
    advancedResults(ResourceType, opts),
    getResourceTypes
)
.post(
    createResourceType
);

router.route('/names')
.get(
    getResourceTypeNames
)

router.route('/:type/calc')
.get(
    calculateResourceType
);

router.route('/:name')
.get(
    getResourceType
)
.put(
    updateResourceType
)
.delete(
    deleteResourceType
)

module.exports = router;