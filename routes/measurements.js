const express = require('express');
const advancedResults = require('../middleware/advancedResults');

const Measurement = require('../models/Measurement');

// Bring each method from measurement
const { 
    getMeasurements,
    getMeasurement,
    createMeasurement,
    updateMeasurement,
    deleteMeasurement
 } 
    = require('../controllers/measurement');

// mergeParams merges the rerouted params
const router = express.Router({ mergeParams: true });

router.route('/')
.get(
    advancedResults(Measurement),
    getMeasurements
)
.post(
    createMeasurement
);

router.route('/:name')
.get(
    getMeasurement
)
.put(
    updateMeasurement
)
.delete(
    deleteMeasurement
)

module.exports = router;