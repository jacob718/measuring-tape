// This is an extension of the default error class that allows
// for a more customizable response
class ErrorResponse extends Error {
    constructor(message, statusCode) {
        //super accesses the parent classe (Error)
        super(message);
        this.statusCode = statusCode;
    }
}

module.exports = ErrorResponse;