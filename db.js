const mongoose = require('mongoose');

const connectDB = async () => {
    // Allow mongoose to connect
    // Second parameter = options
    const conn = await mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    });

    console.log(`MongoDB Connected ${conn.connection.host}`);
}

module.exports = connectDB;