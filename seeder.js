const fs = require('fs');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const colors = require('colors')

// Load env vars
dotenv.config({ path: './config/config.env' });

// Connect to DB
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
});

// Load models
const ResourceType = require('./models/ResourceType');
const Measurement = require('./models/Measurement');

// Read series JSON files
const resourceType = JSON.parse(fs.readFileSync(`${__dirname}/_data/resourceType.json`, 'utf-8'));
const measurement = JSON.parse(fs.readFileSync(`${__dirname}/_data/measurement.json`, 'utf-8'));

// Import into DB
const importData = async () => {
    try {
        await ResourceType.create(resourceType);
        await Measurement.create(measurement);
                
        console.log('Data Imported...'.green.inverse);
        process.exit();
    } catch (err) {
        console.error(err);
    }
}

// Delete data
const deleteData = async () => {
    try {
        // Mongoose functionality that deletes all
        await ResourceType.deleteMany();
        await Measurement.deleteMany();

        console.log('Data Destroyed...'.red.inverse);
        process.exit();
    } catch (err) {
        console.error(err);
    }
}

// In terminal, if we run 'node seeder -i' import data
// 'node seeder -d' delete data
if(process.argv[2] === '-i') {
    importData();
} else if (process.argv[2] === '-d') {
    deleteData();
};