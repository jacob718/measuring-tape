const mongoose = require('mongoose');
const slugify = require('slugify');

const ResourceTypeSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            unique: true
        },
        shorthand: String,
        slug: String,
        description: {
            type: String
        },
        varNames: [ String ],
        varMeas: [String],
        calculator: {
            type: String
        },
        measurement: {
            type: String
        }
    });

    // Create sermon slug from the name before document is saved
    ResourceTypeSchema.pre('save', async function(next) {
        this.slug = slugify(this.name, { lower: true });

        if(!this.shorthand) {
            this.shorthand = this.name.toLowerCase();
        }
        next();
    });

module.exports = mongoose.model('ResourceType', ResourceTypeSchema);