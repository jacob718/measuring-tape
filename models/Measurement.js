const mongoose = require('mongoose');
const slugify = require('slugify');

const MeasurementSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            unique: true
        },
        slug: String,
        singular: {
            type: String
        },
        multiple: {
            type: String
        },
        description: {
            type: String
        },
        sequence: {
            type: Number
        }
    });

    // Create sermon slug from the name before document is saved
    MeasurementSchema.pre('save', async function(next) {
        this.slug = slugify(this.name, { lower: true });

        next();
    });

module.exports = mongoose.model('Measurement', MeasurementSchema);