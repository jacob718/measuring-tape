const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const morgan = require('morgan');
const connectDB = require('./db');

// Load env vars
dotenv.config({ path: './config/config.env' });

connectDB();

// Route files
const healthCheck = require('./routes/healthcheck');
const resourceTypes = require('./routes/resourceTypes');
const measurements = require('./routes/measurements');

// Create server
const app = express();

app.use(cors());
app.use(express.json());

// Development logging middleware
if(process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// Mount routers to specific url
app.use('/healthcheck', healthCheck);
app.use('/resourcetype', resourceTypes);
app.use('/measurement', measurements);

const PORT = process.env.PORT || 5000;

const server = app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`));

// Handle unhandled promise rejections. Listen for the specific event "Unhandled rejection"
process.on('unhandledRejection', (err, promise) => {
    console.log(`Error: ${err.message}`.red)
    // Close server, exit process
    server.close(() => process.exit(1));
});